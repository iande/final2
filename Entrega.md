Ian de Miguel Rodríguez (i.demiguel.2022@alumnos.urjc.es)
Vídeo: https://youtu.be/UU4eDxPrz0w

Requisitos mínimos implementados:

Función change_colors
Función rotate_right
Función mirror
Función rotate_colors
Función blur
Función shift
Función crop
Función grayscale
Función filter
Función main

Programa transforms.py
Programa transform_simple.py
Programa transform_args.py
Programa transform_multi.py

Requisitos opcionales implementados:

Función sepia
Función negative