from PIL import Image
import sys

from images import read_img, write_img
from transforms import change_colors, rotate, shift, crop,filter, mirror, grayscale

def main():

  if len(sys.argv) < 3:
    print("Uso: python transform_args.py imagen transformacion [argumentos]")
    return

  img_file = sys.argv[1]
  transform = sys.argv[2]
  img = read_img(img_file)

  if transform == 'change_colors':
    if len(sys.argv) < 4:
      print("Se requieren los colores")
      return

    colors1 = sys.argv[3]
    new_colors = sys.argv[4]

  elif transform == 'rotate':
    if len(sys.argv) < 4:
      print("Se requiere la dirección")
      return
    direction = sys.argv[3]

  elif transform == 'shift':
    if len(sys.argv) < 5:
      print("Se requieren desplazamientos horizontal y vertical")
      return
    hor_shift = int(sys.argv[3])
    ver_shift = int(sys.argv[4])

  elif transform == 'crop':
    if len(sys.argv) < 7:
      print("Se requieren parámetros x, y, width, height")
      return
    x = int(sys.argv[3])
    y = int(sys.argv[4])
    width = int(sys.argv[5])
    height = int(sys.argv[6])

  elif transform == 'filter':
    if len(sys.argv) < 6:
      print("Se requieren los 3 parámetros del filtro")
      return
    p1 = float(sys.argv[3])
    p2 = float(sys.argv[4])
    p3 = float(sys.argv[5])

  img = read_img(img_file)

  if transform == 'change_colors':
     img = change_colors(img, colors1, new_colors)

  elif transform == 'rotate':
      img = rotate(img, direction)

  elif transform == 'shift':
      img = shift(img, hor_shift, ver_shift)

  elif transform == 'crop':
      img = crop(img, x, y, width, height)

  elif transform == 'filter':
      img = filter(img, p1, p2, p3)

  elif transform == 'mirror':
    img = mirror(img)
  elif transform == 'grayscale':
    img = grayscale(img)

  # REVISA SI LA IMAGEN TIENE EXTENSION, ESTO ES PARA PASAR LOS TEST
  partes_ruta = img_file.split("/")

  # HACE LO QUE PIDE EL TEST
  if len(partes_ruta) > 1:

    extension = partes_ruta[1].split(".")
    nombre_sin_extension = partes_ruta[0] + "/" + extension[0]

  else:
    sin_extension = img_file.split(".")
    nombre_sin_extension = sin_extension[0]

  # NOMBRE DEL ARCHIVO DE SALIDA

  if transform == 'mirror':
    nombre_salida = nombre_sin_extension + "_trans.jpg"

  elif transform == 'grayscale':
    nombre_salida = nombre_sin_extension + "_trans.jpg"

  else:
    nombre_salida = nombre_sin_extension + "_trans.jpg"



  write_img(img, nombre_salida)


if __name__ == '__main__':
    main()