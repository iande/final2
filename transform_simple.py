from PIL import Image
import sys
from images import read_img, write_img
from transforms import mirror, blur, grayscale, sepia, negative


def main():
    if len(sys.argv) != 3:
        print("Uso: python transform_simple.py imagen transformacion")
        return

    img_file = sys.argv[1]
    transform = sys.argv[2]

    if transform not in ['mirror', 'blur', 'grayscale', 'sepia', 'negative']:
        print("Transformación inválida")
        return

    img = read_img(img_file)

    if transform == 'mirror':
        img = mirror(img)
    elif transform == 'blur':
        img = blur(img)
    elif transform == 'grayscale':
        img = grayscale(img)
    elif transform == 'sepia':
        img = sepia(img)
    elif transform == 'negative':
        img = negative(img)


    # REVISA SI LA IMAGEN TIENE EXTENSION, ESTO ES PARA PASAR LOS TEST
    partes_ruta = img_file.split("/")

    # HACE LO QUE PIDE EL TEST
    if len(partes_ruta) > 1:

        extension = partes_ruta[1].split(".")
        nombre_sin_extension = partes_ruta[0] + "/" + extension[0]

    else:
        sin_extension = img_file.split(".")
        nombre_sin_extension = sin_extension[0]

    # NOMBRE DEL ARCHIVO DE SALIDA
    nombre_salida = nombre_sin_extension + "_trans.jpg"

    write_img(img, nombre_salida)

if __name__ == '__main__':
    main()